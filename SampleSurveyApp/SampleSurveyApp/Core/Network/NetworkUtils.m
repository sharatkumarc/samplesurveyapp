//
//  NetworkUtils.m
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import "NetworkUtils.h"
#import <AFNetworking.h>
#import "ServerEndPoints.h"
#import "NotificationConstants.h"

NSString * const ACCESS_TOKEN_CONTAINER_KEY = @"AccessTokenData";

@implementation NetworkUtils

static NetworkUtils *this = nil;

+ (id) sharedInstance {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        this = [[NetworkUtils alloc] init];
    });
    
    return this;
}

- (void)validateAccessToken {
    
    NSDictionary *accessTokenData = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_CONTAINER_KEY];

    // Check if access token is avaialble in local storage, if not then request for a new one
    if (accessTokenData == nil) {
        
        NSLog(@"Access Token does not exist, Requesting a new one");
        [self requestNewAccessToken];
    }
    else
    {
        // Check for the expiry of access token and request a new one if required
        NSDate *creationDate = [accessTokenData objectForKey:@"CreationDate"];
        NSLog(@"Access Token Creation Data: %@", creationDate);
        
        NSNumber *expiryTime = [accessTokenData objectForKey:@"ExpiryTime"];
        NSLog(@"Access Token expiry time In secs: %f", [expiryTime doubleValue]);
        
        NSTimeInterval elapsedTime = [[NSDate date] timeIntervalSinceDate:creationDate];
        
        if (elapsedTime > [expiryTime doubleValue]) {
            
            NSLog(@"Elapsed Time: %f therefore access Token Expired, and now requesting a new one", elapsedTime);
            [self requestNewAccessToken];
        }
        else
        {
            NSLog(@"A valid access token found in the cache");
            
            // post valid access token available notification
            [[NSNotificationCenter defaultCenter] postNotificationName:ACCESS_TOKEN_VALID object:nil];
        }
    }
}

- (void)requestNewAccessToken {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSDictionary *parameters = @{@"grant_type": @"password",
                                 @"username": @"carlos@nimbl3.com",
                                 @"password": @"antikera"};
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:ACCESS_TOKEN_URL parameters:parameters error:nil];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
            
            // post access token error
            [[NSNotificationCenter defaultCenter] postNotificationName:ACCESS_TOKEN_ERROR object:error];
            
        } else {
            
            NSDictionary *_accessTokenResponse = (NSDictionary *)responseObject;
            
            if (_accessTokenResponse != NULL) {
                [self verifyAndStoreAccessTokenData:_accessTokenResponse];
            }
        }
    }];
    
    [dataTask resume];
}

- (void)verifyAndStoreAccessTokenData:(NSDictionary *)_accessTokenResponse {
    
    NSString *accessToken = [_accessTokenResponse objectForKey:@"access_token"];
    double expiryTime = [(NSNumber *)[_accessTokenResponse objectForKey:@"expires_in"] doubleValue];
    double creationTime = [(NSNumber *)[_accessTokenResponse objectForKey:@"created_at"] doubleValue];

    //Check if the response data is valid
    //Todo: Validation logic of creationTime is not proper, it needs to be verified if its a valid date :)
    if (accessToken != NULL && expiryTime > 0 && creationTime > 0) {
        
        NSDate *creationDate = [NSDate dateWithTimeIntervalSince1970:creationTime];
        
        NSDictionary *accessTokenData = @{@"CreationDate": creationDate,
                                          @"ExpiryTime": [NSNumber numberWithDouble:expiryTime],
                                          @"AccessToken": accessToken};
        
        NSLog(@"New Access token details :%@", accessTokenData);
        
        [[NSUserDefaults standardUserDefaults] setObject:accessTokenData forKey:ACCESS_TOKEN_CONTAINER_KEY];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ACCESS_TOKEN_VALID object:nil];
    }
    else
    {
        NSDictionary *error = @{@"error": @"Invalid Access token data from server"};
        [[NSNotificationCenter defaultCenter] postNotificationName:ACCESS_TOKEN_ERROR object:error];
    }
}

- (NSString *)getAccessToken {
    
    NSDictionary *accessTokenData = [[NSUserDefaults standardUserDefaults] objectForKey:ACCESS_TOKEN_CONTAINER_KEY];
    NSString *accessToken = [accessTokenData objectForKey:@"AccessToken"];

    return accessToken;
}

@end
