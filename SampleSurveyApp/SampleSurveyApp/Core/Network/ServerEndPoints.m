//
//  ServerEndPoints.m
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import "ServerEndPoints.h"

NSString * const SURVEY_URL = @"https://nimbl3-survey-api.herokuapp.com/surveys.json";
NSString * const ACCESS_TOKEN_URL = @"https://nimbl3-survey-api.herokuapp.com/oauth/token";
