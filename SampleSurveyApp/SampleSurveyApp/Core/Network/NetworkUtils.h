//
//  NetworkUtils.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

//  ************************    Description and Responsibilities    ************************ //
//  This class is responsible for handling all network related stuff
//  It can make use of 3rd party libraries (such as AFNetworking) for performing HTTP operations.
//  It is Singleton class and will be accessed inside classes who need the network services
//  It will return the response in a callback as Raw Data (NSData) or in a JSON format; incase of an error it will return the error code and error description to the requesting class or function
//  It will not parse the response data and create data models, this will be the responsibilty of the classes requesting the service

#import <Foundation/Foundation.h>

@interface NetworkUtils : NSObject

+ (id)sharedInstance;

- (void)validateAccessToken;

@end
