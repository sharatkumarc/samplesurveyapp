//
//  DataManager.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

//  ************************    Description and Responsibilities    ************************ //
//  This class is responsible for storing and retrieving app related data, user profile data and preferences if any.
//  It can persist data inside NSUSerDefaults or using any other third party library that provides data persistence services.
//  It can also make use of iOS keychain services and Core Data depending upon the type of Data.
//  It is Singleton class and will be accessed inside classes who need to access data.
//  It will make use of Network Manager instance incase data needs to be data download from server.
//  It may not create and return very specific data models needed by the requesting object. Sometimes it may do so depending upon the type of data and the frequency at which it is accessed and modified.


//  For Example: Incase of Survey Data that is downloaded from the URL, will be returned as objects that will contain the complete information of one single survey.
//  SURVEY LIST SCREEN: It will need the data models that will only contain 'Title of the Survey' , 'Decription of the Survey' , 'URL for the Cover Image' and 'Survey Id'.
//  SURVEY SCREEN: It will need data models that will hold information about questions, feedback text and other description text
//  There needs to be some other class that will make use of the SURVEY DATA and create above data models.

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

+ (id) sharedInstance;

@end
