//
//  DataManager.m
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

static DataManager *this = nil;

+ (id) sharedInstance {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        this = [[DataManager alloc] init];
    });
    
    return this;
}

@end
