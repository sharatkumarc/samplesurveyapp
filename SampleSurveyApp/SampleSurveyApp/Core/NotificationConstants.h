//
//  NotificationConstants.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString * const ACCESS_TOKEN_ERROR;
FOUNDATION_EXPORT NSString * const ACCESS_TOKEN_VALID;
FOUNDATION_EXPORT NSString * const SURVEY_DATA_FETCHED;
FOUNDATION_EXPORT NSString * const UPDATE_SURVEY_VIEW;
