//
//  NotificationConstants.m
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import "NotificationConstants.h"

NSString * const ACCESS_TOKEN_ERROR     = @"Access Token Error";
NSString * const ACCESS_TOKEN_VALID     = @"Access Token Valid";
NSString * const SURVEY_DATA_FETCHED    = @"Survey Data Fetched";
NSString * const UPDATE_SURVEY_VIEW     = @"Update Survey View";
