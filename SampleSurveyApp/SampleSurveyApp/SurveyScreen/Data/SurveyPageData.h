//
//  SurveyListItemData.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 27/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//
//  ************************    Description and Responsibilities    ************************ //
//  Holds the data for survey list item.


#import <Foundation/Foundation.h>

@interface SurveyPageData : NSObject

@property (nonatomic) NSString *surveyTitle;
@property (nonatomic) NSString *coverImageURL;
@property (nonatomic) NSString *surveyDescription;
@property (nonatomic) NSString *surveyId;

@end
