//
//  SurveyScreenData.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//
//  ************************    Description and Responsibilities    ************************ //
//  Holds the data for survey.

#import <Foundation/Foundation.h>

@interface SurveyScreenData : NSObject

@end
