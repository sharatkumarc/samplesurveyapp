//
//  SurveyDataManager.m
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 27/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import "SurveyDataManager.h"
#import <AFNetworking.h>
#import "ServerEndPoints.h"
#import "NotificationConstants.h"
#import "SurveyPageData.h"

@interface SurveyDataManager ()

@property (readonly, strong, nonatomic) NSArray *surveyDataObject;

@end

@implementation SurveyDataManager

@synthesize surveyDataObject = _surveyDataObject;

#pragma mark - Instance Methods

static SurveyDataManager *this = nil;

+ (id) sharedInstance {
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        this = [[SurveyDataManager alloc] init];
    });
    
    return this;
}

- (void)fetchSurveyData {
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        [self downloadSurveyData];
    });
}

- (void)downloadSurveyData {
    
    // Surveys data will be fetched inside this method
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSDictionary *parameters = @{@"access_token": @"d9584af77d8c0d6622e2b3c554ed520b2ae64ba0721e52daa12d6eaa5e5cdd93"};
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:SURVEY_URL parameters:parameters error:nil];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            
            _surveyDataObject = responseObject;
            
            [self processSurveyData];
        }
    }];
    [dataTask resume];
}

- (NSArray *)getSurveyPageDataObject
{
    NSMutableArray *surveyPageData = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < _surveyDataObject.count; i++) {
        
        SurveyPageData *pageDataObject = [[SurveyPageData alloc] init];
        
        pageDataObject.surveyTitle = [(NSDictionary*)_surveyDataObject[i] objectForKey:@"title"];
        pageDataObject.surveyDescription = [(NSDictionary*)_surveyDataObject[i] objectForKey:@"description"];
        pageDataObject.coverImageURL = [(NSDictionary*)_surveyDataObject[i] objectForKey:@"cover_image_url"];
        pageDataObject.coverImageURL = [pageDataObject.coverImageURL stringByAppendingString:@"l"];
        
        [surveyPageData addObject:pageDataObject];
        
        NSLog(@"Survey Data Object %@", _surveyDataObject[i]);

    }
    return surveyPageData;
}

- (void)processSurveyData {
    
    // download cover images
    [[NSNotificationCenter defaultCenter] postNotificationName:SURVEY_DATA_FETCHED object:nil];
}

@end
