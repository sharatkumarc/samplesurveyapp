//
//  SurveyDataManager.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 27/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//
//  ************************    Description and Responsibilities    ************************ //
//  This class is responsible for parsing and creating data models from the Raw Data (Survey Data) received from DataManager
//  It will used by SurveyTaskManager to request data models for various survey screens

#import <Foundation/Foundation.h>

@interface SurveyDataManager : NSObject

+ (id) sharedInstance;

- (void)fetchSurveyData;

- (NSArray *)getSurveyPageDataObject;

@end
