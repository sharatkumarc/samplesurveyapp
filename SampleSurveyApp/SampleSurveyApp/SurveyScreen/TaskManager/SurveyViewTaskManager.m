//
//  SurveyViewTaskManager.m
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import "SurveyViewTaskManager.h"
#import "SurveyDataManager.h"
#import "SurveyPageViewController.h"
#import "NotificationConstants.h"

@interface SurveyViewTaskManager ()

@property (readonly, strong, nonatomic) NSArray *surveyPageDataObject;
@end

@implementation SurveyViewTaskManager

@synthesize surveyPageDataObject = _surveyPageDataObject;

- (void) requestSurveyData {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadSurveyData)
                                                 name:SURVEY_DATA_FETCHED
                                               object:nil];
    
    [[SurveyDataManager sharedInstance] fetchSurveyData];
}

- (void) loadSurveyData {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SURVEY_DATA_FETCHED object:nil];
    
    _surveyPageDataObject = [[SurveyDataManager sharedInstance] getSurveyPageDataObject];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_SURVEY_VIEW object:nil];
}

- (SurveyPageViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard {
    // Return the data view controller for the given index.
    if (([_surveyPageDataObject count] == 0) || (index >= [_surveyPageDataObject count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    SurveyPageViewController *pageViewController = [storyboard instantiateViewControllerWithIdentifier:@"SurveyPageViewController"];
    pageViewController.dataObject = _surveyPageDataObject[index];
    pageViewController.view.tag = index;
    return pageViewController;
}

- (NSInteger)surveyPageCount {
    return [_surveyPageDataObject count];
}

- (NSUInteger)indexOfViewController:(SurveyPageViewController *)viewController {
    // Return the index of the given data view controller.
    // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [_surveyPageDataObject indexOfObject:viewController.dataObject];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(SurveyPageViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(SurveyPageViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [_surveyPageDataObject count]) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

@end
