//
//  SurveyViewTaskManager.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 28/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//
//  ************************    Description and Responsibilities    ************************ //
//  This class is the input processing unit of Survey screens
//  It will used by ViewControllers for perform necessary tasks on user input
//  It will also provide required data to views via ViewControllers

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class SurveyPageViewController;

@interface SurveyViewTaskManager : NSObject <UIPageViewControllerDataSource>

- (void)requestSurveyData;
- (NSInteger)surveyPageCount;

- (SurveyPageViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(SurveyPageViewController *)viewController;

@end
