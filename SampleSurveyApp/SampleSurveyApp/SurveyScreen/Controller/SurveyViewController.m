//
//  SurveyViewController.m
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 26/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import "SurveyViewController.h"
#import "SurveyViewTaskManager.h"
#import "NetworkUtils.h"
#import "NotificationConstants.h"
#import "SurveyPageViewController.h"

@interface SurveyViewController ()
 
@property (readonly, strong, nonatomic) SurveyViewTaskManager *svTaskManager;
@property (readonly, strong, nonatomic) UIPageControl *pageControl;

@end

@implementation SurveyViewController

@synthesize svTaskManager = _svTaskManager;
@synthesize pageControl = _pageControl;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initialise];
}

- (void) initialise {
    
    [self subscribeForNotifications];
    
    // Trigger loading animation
    [self.LoadingText setHidden:NO];
    [self.loadingAnimation startAnimating];
    
    // Validate access token
    [[NetworkUtils sharedInstance] validateAccessToken];
}

- (void)subscribeForNotifications {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showAccessTokenError:)
                                                 name:ACCESS_TOKEN_ERROR
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onValidAccessTokenAvailable)
                                                 name:ACCESS_TOKEN_VALID
                                               object:nil];
}

- (void)unsubscribeFromNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ACCESS_TOKEN_ERROR object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ACCESS_TOKEN_VALID object:nil];
}

- (void)showAccessTokenError:(NSNotification *)notification {
    
    NSDictionary *error = (NSDictionary *)notification.object;
    NSLog(@"Access token Error : %@", error);
}

- (void)onValidAccessTokenAvailable {
    
    // Start fetching survey data
    _svTaskManager = [[SurveyViewTaskManager alloc] init];
    
    [self refreshData];
}

- (void)refreshData {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onSurveyDataAvailable)
                                                 name:UPDATE_SURVEY_VIEW
                                               object:nil];
    [_svTaskManager requestSurveyData];
}

- (void)onSurveyDataAvailable {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UPDATE_SURVEY_VIEW object:nil];
    [self initializePageViewController];
    
    [self setupPageControl];
    
    [self.LoadingText setHidden:YES];
    [self.loadingAnimation stopAnimating];
}

- (void)initializePageViewController {
    
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationVertical options:nil];
    
    self.pageViewController.delegate = self;
    
    SurveyPageViewController *startingViewController = [_svTaskManager viewControllerAtIndex:0 storyboard:self.storyboard];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    self.pageViewController.dataSource = _svTaskManager;
    
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    
    // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
    CGRect pageViewRect = self.view.bounds;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        pageViewRect = CGRectInset(pageViewRect, 40.0, 40.0);
    }
    self.pageViewController.view.frame = pageViewRect;
    
    [self.pageViewController didMoveToParentViewController:self];
}

- (void)setupPageControl {
    
    _pageControl = [[UIPageControl alloc] init];
    _pageControl.transform = CGAffineTransformMakeRotation(M_PI_2);

    float posX = self.view.bounds.origin.x + self.view.bounds.size.width - 20;
    float posY = self.view.bounds.origin.y + self.view.bounds.size.height/2;
    float width = 10;
    float height = 0;
    
    _pageControl.frame = CGRectMake(posX, posY, width, height);
    _pageControl.numberOfPages = [_svTaskManager surveyPageCount];
    _pageControl.currentPage = 0;
    
    [self.view addSubview:_pageControl];
}

- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed) {
        _pageControl.currentPage = ((UIViewController *)self.pageViewController.viewControllers.firstObject).view.tag;;
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:YES];
    [self unsubscribeFromNotifications];
}

- (IBAction)refreshButtonPressed:(id)sender {
    
    [self.pageViewController removeFromParentViewController];
    [self.pageViewController.view removeFromSuperview];
    
    // Trigger loading animation
    [self.LoadingText setHidden:NO];
    [self.loadingAnimation startAnimating];
    
    [self refreshData];
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
