//
//  SurveyPageViewController.m
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 29/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import "SurveyPageViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation SurveyPageViewController

@synthesize dataObject = _dataObject;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.surveyTitle.text = self.dataObject.surveyTitle;
    self.surveyDescription.text = self.dataObject.surveyDescription;
    
    [self.backgroundImage sd_setImageWithURL:[NSURL URLWithString:self.dataObject.coverImageURL]];
}


- (IBAction)surveyButton:(id)sender {
}
@end
