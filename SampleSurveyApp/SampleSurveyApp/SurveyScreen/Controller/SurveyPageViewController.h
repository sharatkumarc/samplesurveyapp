//
//  SurveyPageViewController.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 29/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SurveyPageData.h"

@interface SurveyPageViewController : UIViewController

@property (strong, nonatomic) SurveyPageData *dataObject;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (weak, nonatomic) IBOutlet UILabel *surveyTitle;
@property (weak, nonatomic) IBOutlet UILabel *surveyDescription;

- (IBAction)surveyButton:(id)sender;

@end
