//
//  SurveyViewController.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 26/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

//  ************************    Description and Responsibilities    ************************ //
//  This class is will be linked to xib files
//  They will have call back methods for user inputs
//  It will also hold reference of other views (Loading Animation)

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString * const SERVER_NAME_SECURE;

@interface SurveyViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

- (IBAction)refreshButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingAnimation;

@property (weak, nonatomic) IBOutlet UILabel *LoadingText;
@end

