//
//  AppDelegate.h
//  SampleSurveyApp
//
//  Created by Sharatkumar Chilaka on 26/11/17.
//  Copyright © 2017 Sharatkumar Chilaka. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

